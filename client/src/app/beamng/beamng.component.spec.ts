import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeamngComponent } from './beamng.component';

describe('BeamngComponent', () => {
  let component: BeamngComponent;
  let fixture: ComponentFixture<BeamngComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeamngComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeamngComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
