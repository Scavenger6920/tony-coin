import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BeamngComponent } from './beamng.component';

@NgModule({
  declarations: [
    BeamngComponent
  ],
  imports: [
    CommonModule
  ]
})
export class BeamngModule { }
