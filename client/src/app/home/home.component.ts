import { HttpClient } from '@angular/common/http';
import { Component, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { lastValueFrom } from 'rxjs';

import { PurchaseDialogComponent } from '../purchase-dialog/purchase-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(
    public dialog: MatDialog,
    private renderer: Renderer2,
    private http: HttpClient
  ) {}

  public openDialog(): void {
    lastValueFrom(this.http.get(window.location.origin + '/api/players'));
    const dialogRef = this.dialog.open(PurchaseDialogComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      const tonys = [{
        name: '$TONY-Crouch.png',
        path: '/assets/tony-coin-crouch.png',
      }, {
        name: '$TONY-Goblin.png',
        path: '/assets/tony-coin-goblin.png'
      }];
      const item = tonys[Math.floor(Math.random() * tonys.length)];
      const link = this.renderer.createElement('a');
      link.setAttribute('target', '_blank');
      link.setAttribute('href', item.path);
      link.setAttribute('download', item.name);
      link.click();
      link.remove();
    })
  }
}
