const express = require('express');
const app = express();
const players = require('./api/players');
require('dotenv').config();

const env = process.env.NODE_ENV;
const port = env === 'prod' ? process.env.PROD_PORT : process.env.DEV_PORT;

app.use('/api/players', players);
app.use('/', express.static('dist/tonycoin'));

app.listen(port);
console.log('Server started at http://localhost:' + port);
